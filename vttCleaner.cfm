<cfscript>
fileDir = directoryList(path=".", recurse=false, filter="*.mp4", type="file", listinfo="name");
arrayEach(fileDir, function(item, ind){ 
    vttName = Trim( lCase( ListFirst(item, '.') ) ) & ".vtt";
    script = " -y -i '#item#' -map 0:s:0 '#vttName#'";
    try {
    cfexecute( name="ffmpeg" arguments="#script#" timeout="60");
    } catch ( any e ) {
        tmpName = Replace( Replace( Replace( item, " ", "\ ", "ALL" ), "(", "\(" ), ")", "\)" );
        script = " -y -f lavfi -i 'movie=#tmpName#[out+subcc]' -map 0:1 '#vttName#'";
        cfexecute( name="ffmpeg" arguments="#script#");
    }

    newFileText = "";
    file = fileOpen(vttName, "read");
    try {
    prevLine = "";
    lineBuffer = [];
    newFileText = fileReadLine(file); // first line has the file type, need to skip it
    newFileText = newFileText & chr(10) & fileReadLine(file); // second line is blank, skip it.
    lineBuffer.append( fileReadLine(file) );
    lineNumber = 1;
    while ( !fileIsEOF(file) ) {
        if ( lineBuffer.len() EQ 9 ) {
            trimToLine = 3;
            if ( ListLen(lineBuffer[4], "-->") GT 1 AND Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listLast(lineBuffer[4], "-->")) AND Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listFirst(lineBuffer[7], "-->"))) {
                lineBuffer[4] = lineBuffer[7];
                lineBuffer.deleteAt(7);
                lineBuffer.deleteAt(6);
                trimToLine = lineBuffer.len();
            } else if ( ListLen(lineBuffer[4], "-->") GT 1 AND (Trim(ListLast(lineBuffer[1], "-->" )) IS Trim(listFirst(lineBuffer[4], "-->")) OR Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listLast(lineBuffer[4], "-->"))) ) {
                lineBuffer[1] = Trim(ListFirst(lineBuffer[1], "-->" )) & " --> " & Trim(listLast(lineBuffer[4], "-->"));
                lineBuffer[3] = lineBuffer[5];
                lineBuffer.deleteAt(5);
                lineBuffer.deleteAt(4);
                trimToLine = 4;
            } else {
            }
            if ( trimToLine EQ lineBuffer.len() ) { 
                newFileText = newFileText & chr(10) & lineBuffer.toList( chr(10) );
                lineBuffer = []; 
            } else {
                prePendBuffer = [];
                for (i=trimToline; i GT 0; i-- ) {
                    prePendBuffer.prepend( lineBuffer[i] );
                    lineBuffer.deleteAt(i);
                }
                newFileText = newFileText & chr(10) & prePendBuffer.toList( chr(10) );
            }/* */
        }
        lineNumber++;
        lineBuffer.append( fileReadLine(file) );
    }
    if ( lineBuffer.len() GTE 5 ) {
        if ( lineBuffer.len() GTE 7 AND Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listLast(lineBuffer[4], "-->")) AND Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listFirst(lineBuffer[7], "-->"))) {
            lineBuffer[4] = lineBuffer[7];
            lineBuffer.deleteAt(7);
            lineBuffer.deleteAt(6);
            trimToLine = lineBuffer.len();
        } else if ( ListLen(lineBuffer[4], "-->") GT 1 AND (Trim(ListLast(lineBuffer[1], "-->" )) IS Trim(listFirst(lineBuffer[4], "-->")) OR Trim(ListFirst(lineBuffer[4], "-->" )) IS Trim(listLast(lineBuffer[4], "-->"))) ) {
            lineBuffer[1] = Trim(ListLast(lineBuffer[1], "-->" )) & " --> " & Trim(listFirst(lineBuffer[4], "-->"));
            lineBuffer[3] = lineBuffer[5];
            lineBuffer.deleteAt(5);
            lineBuffer.deleteAt(4);
            trimToLine = 5;
        } 
    }
    newFileText = ReplaceNoCase( newFileText & chr(10) & lineBuffer.toList( chr(10) ) , "\h", " ", "ALL" );
    fileClose(file);
    } catch( any e ) { fileClose(file); WriteDump( e ); }
    fileWrite( vttName&".txt", newFileText );
    outFile = ReplaceNoCase( item, '.mp4', '.m4v' );
    script = " -y -i '#item#' -i '#vttName#.txt' -map 0 -c copy -map -0:s -map -0:d -c:s mov_text -map 1:0 '#outFile#'";
    cfexecute( name="ffmpeg" arguments="#script#" );
});
</cfscript>
